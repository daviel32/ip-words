function getWords(ignoreIP){
  var aIp = [];
  aIp[0] = document.querySelector("#ip1").value;
  aIp[1] = document.querySelector("#ip2").value;
  aIp[2] = document.querySelector("#ip3").value;
  aIp[3] = document.querySelector("#ip4").value;

  if(ValidateIPaddress(aIp[0] + "." + aIp[1] + "." + aIp[2] + "." + aIp[3]) && !ignoreIP)
  {
    document.querySelector("#words1").value = words[(parseInt(aIp[0]) << 8) + parseInt(aIp[1])];
    document.querySelector("#words2").value = words[(parseInt(aIp[2]) << 8) + parseInt(aIp[3])];
  }
  else
  {
    var word1 = document.querySelector("#words1").value;
    var word2 = document.querySelector("#words2").value;

    var num1 = wordsIndex[word1];
    var num2 = wordsIndex[word2];

    document.querySelector("#ip1").value = num1 >> 8;
    document.querySelector("#ip2").value = (num1 & 0xFF);

    document.querySelector("#ip3").value = num2 >> 8;
    document.querySelector("#ip4").value = (num2 & 0xFF);
  }
}

function parseIp(ipObj, setVal){
  var val = ipObj.value;
  if(typeof(val) == "undefined") val = 0;

  val = val.replace(/[^0-9]+/g, '');
  val = parseInt(val);
  if(val > 255) val = 255;
  if(val < 0) val = "";
  if(isNaN(val)) val = "";
  if(setVal == true) ipObj.value = val;
  return val;
}

function increaseNumber(ipObj){
  var val = parseIp(ipObj, false);
  if(val === "") val = 0;
  else val += 1
  if(val > 255) val = 0;
  ipObj.value = val;
  return val;
}

function decreaseNumber(ipObj){
  var val = parseIp(ipObj, false);
  if(val === "") val = 0;
  else val -= 1
  if(val < 0) val = 255;
  ipObj.value = val;
  return val;
}

function swap(json){
  var ret = {};
  for(var key in json){
  ret[json[key]] = key;
  }
  return ret;
}

function ValidateIPaddress(ipaddress)
{
  if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))
  {
    return (true)
  }
  return (false)
}

function parsePaste(ev){
  ev.preventDefault();
  clipboardData = ev.clipboardData || window.clipboardData;
  pastedData = clipboardData.getData('Text');

  if(ValidateIPaddress(pastedData)){
    var aIp = pastedData.split(".");

    document.querySelector("#ip1").value = aIp[0];
    document.querySelector("#ip2").value = aIp[1];
    document.querySelector("#ip3").value = aIp[2];
    document.querySelector("#ip4").value = aIp[3];
    getWords(false);
  } else {
    var words = pastedData.split(" ");
    if(words.length >= 2){
      document.querySelector("#words1").value = words[0];
      document.querySelector("#words2").value = words[1];
      getWords(true);
    }
  }
}


if(typeof(localStorage['wordsIndex']) == "undefined"){
  wordsIndex = swap(words);
  localStorage['wordsIndex'] = JSON.stringify(wordsIndex);
}
else{
  wordsIndex = JSON.parse(localStorage['wordsIndex']);
}


document.addEventListener('DOMContentLoaded', function(){
  document.querySelector("#ip1").addEventListener("keyup", function(ev){
    var input = document.querySelector("#ip1");
    if(ev.keyCode == 37){
      // arrow key left
      //document.querySelector("#ip4").focus()
    }
    else if(ev.keyCode == 39){
      // arrow key right
      //document.querySelector("#ip2").focus()
    }
    else if(ev.keyCode == 38){
      // arrow key up
      increaseNumber(input);
    }
    else if(ev.keyCode == 40){
      // arrow key down
      decreaseNumber(input);
    }
    if(ev.ctrlKey != true){
      var val = parseIp(input);
      if(val.toString().length == 3 && ev.keyCode >= 65) document.querySelector("#ip2").focus()
      getWords(false);
    }
  });

  document.querySelector("#ip2").addEventListener("keyup", function(ev){
    var input = document.querySelector("#ip2");
    if(ev.keyCode == 37){
      // arrow key left
      //document.querySelector("#ip1").focus()
    }
    else if(ev.keyCode == 39){
      // arrow key right
      //document.querySelector("#ip3").focus()
    }
    else if(ev.keyCode == 38){
      // arrow key up
      increaseNumber(input);
    }
    else if(ev.keyCode == 40){
      // arrow key down
      decreaseNumber(input);
    }
    if(ev.ctrlKey != true){
      var val = parseIp(input);
      if(val.toString().length == 3 && ev.keyCode >= 65) document.querySelector("#ip3").focus()
      getWords(false);
    }
  });

  document.querySelector("#ip3").addEventListener("keyup", function(ev){
    var input = document.querySelector("#ip3");
    if(ev.keyCode == 37){
      // arrow key left
      //document.querySelector("#ip2").focus()
    }
    else if(ev.keyCode == 39){
      // arrow key right
      //document.querySelector("#ip4").focus()
    }
    else if(ev.keyCode == 38){
      // arrow key up
      increaseNumber(input);
    }
    else if(ev.keyCode == 40){
      // arrow key down
      decreaseNumber(input);
    }
    if(ev.ctrlKey != true){
      var val = parseIp(input);
      if(val.toString().length == 3 && ev.keyCode >= 65) document.querySelector("#ip4").focus()
      getWords(false);
    }
  });

  document.querySelector("#ip4").addEventListener("keyup", function(ev){
    var input = document.querySelector("#ip4");
    if(ev.keyCode == 37){
      // arrow key left
      //document.querySelector("#ip3").focus()
    }
    else if(ev.keyCode == 39){
      // arrow key right
      //document.querySelector("#ip1").focus()
    }
    else if(ev.keyCode == 38){
      // arrow key up
      increaseNumber(input);
    }
    else if(ev.keyCode == 40){
      // arrow key down
      decreaseNumber(input);
    }
    if(ev.ctrlKey != true){
      parseIp(input);
      getWords(false);
    }
  });

  document.querySelector("#words1").addEventListener("keyup", function(ev){
    if(ev.ctrlKey != true){
      var wordsInput = document.querySelector("#words1");
      var val = wordsInput.value;
      val = val.replace(/[^a-zA-Z]+/g, '');
      wordsInput.value = val;
      getWords(true);
    }
  });

  document.querySelector("#words2").addEventListener("keyup", function(ev){
    if(ev.ctrlKey != true){
      var wordsInput = document.querySelector("#words2");
      var val = wordsInput.value;
      val = val.replace(/[^a-zA-Z]+/g, '');
      wordsInput.value = val;
      getWords(true);
    }
  });

  document.querySelector("#ip1").addEventListener ("paste", function(ev){
    parsePaste(ev);
  }, false);

  document.querySelector("#ip2").addEventListener ("paste", function(ev){
    parsePaste(ev);
  }, false);

  document.querySelector("#ip3").addEventListener ("paste", function(ev){
    parsePaste(ev);
  }, false);

  document.querySelector("#ip4").addEventListener ("paste", function(ev){
    parsePaste(ev);
  }, false);

  document.querySelector("#words1").addEventListener ("paste", function(ev){
    parsePaste(ev);
  }, false);

  document.querySelector("#words2").addEventListener ("paste", function(ev){
    parsePaste(ev);
  }, false);

  document.querySelector("#reset").addEventListener("click", function(ev){
    document.querySelector("#ip1").value = 0;
    document.querySelector("#ip2").value = 0;
    document.querySelector("#ip3").value = 0;
    document.querySelector("#ip4").value = 0;

    document.querySelector("#words1").value = "";
    document.querySelector("#words2").value = "";
  });

}, false);
